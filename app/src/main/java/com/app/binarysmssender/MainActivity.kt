package com.app.binarysmssender

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.telephony.SmsManager
import android.content.Intent
import android.app.PendingIntent
import android.content.IntentFilter
import android.widget.Toast
import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val SMS_SENT = "SMS_SENT"
        val SMS_DELIVERED = "SMS_DELIVERED"

        val sentPendingIntent = PendingIntent.getBroadcast(this, 0, Intent(SMS_SENT), 0)
        val deliveredPendingIntent = PendingIntent.getBroadcast(this, 0, Intent(SMS_DELIVERED), 0)


        registerReceiver(object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                when (resultCode) {
                    Activity.RESULT_OK -> Toast.makeText(context, "SMS sent successfully", Toast.LENGTH_SHORT).show()
                    SmsManager.RESULT_ERROR_GENERIC_FAILURE -> Toast.makeText(
                        context,
                        "Generic failure cause",
                        Toast.LENGTH_SHORT
                    ).show()
                    SmsManager.RESULT_ERROR_NO_SERVICE -> Toast.makeText(
                        context,
                        "Service is currently unavailable",
                        Toast.LENGTH_SHORT
                    ).show()
                    SmsManager.RESULT_ERROR_NULL_PDU -> Toast.makeText(
                        context,
                        "No pdu provided",
                        Toast.LENGTH_SHORT
                    ).show()
                    SmsManager.RESULT_ERROR_RADIO_OFF -> Toast.makeText(
                        context,
                        "Radio was explicitly turned off",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }, IntentFilter(SMS_SENT))

// For when the SMS has been delivered
        registerReceiver(object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                when (resultCode) {
                    Activity.RESULT_OK -> Toast.makeText(baseContext, "SMS delivered", Toast.LENGTH_SHORT).show()
                    Activity.RESULT_CANCELED -> Toast.makeText(
                        baseContext,
                        "SMS not delivered",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }, IntentFilter(SMS_DELIVERED))


        PermissionUtils.requestPermission(this)


        if(PermissionUtils.checkPermission(this)) {

            val smsManager = SmsManager.getDefault()

            val phoneNumber = "+12345678"
            val smsBody = "SMS BODY".toByteArray()
            val port: Short = 5499

// Send a text based SMS
            smsManager.sendDataMessage(phoneNumber, null, port, smsBody, sentPendingIntent, deliveredPendingIntent)
        }

    }
}
