package com.app.binarysmssender;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;


import static android.Manifest.permission.*;

public class PermissionUtils {

    static public final int PERMISSION_REQUEST_CODE=200;
    public static boolean checkPermission(Context context) {
//        int result0 = ContextCompat.checkSelfPermission(context.getApplicationContext(), CALL_PHONE);
//        int result1 = ContextCompat.checkSelfPermission(context.getApplicationContext(), READ_CONTACTS);
//        int result2 = ContextCompat.checkSelfPermission(context.getApplicationContext(), READ_EXTERNAL_STORAGE);
        int result3 = ContextCompat.checkSelfPermission(context.getApplicationContext(), SEND_SMS);
//        int result4 = ContextCompat.checkSelfPermission(context.getApplicationContext(), RECORD_AUDIO);
//        int result5 = ContextCompat.checkSelfPermission(context.getApplicationContext(), ADD_VOICEMAIL);
//        int result6 = ContextCompat.checkSelfPermission(context.getApplicationContext(), READ_PHONE_STATE);




        return result3  == PackageManager.PERMISSION_GRANTED ;

//        return result0 == PackageManager.PERMISSION_GRANTED &&
//                result1 == PackageManager.PERMISSION_GRANTED &&
//                result2 == PackageManager.PERMISSION_GRANTED &&
//                result3 == PackageManager.PERMISSION_GRANTED &&
//                result4 == PackageManager.PERMISSION_GRANTED &&
//                result5 == PackageManager.PERMISSION_GRANTED
//              && result6 == PackageManager.PERMISSION_GRANTED
//                ;

    }

    public static void requestPermission(Context context) {
        //CALL_PHONE,READ_CONTACTS,READ_EXTERNAL_STORAGE,,RECORD_AUDIO,ADD_VOICEMAIL , READ_PHONE_STATE

        ActivityCompat.requestPermissions((Activity) context , new String[]{SEND_SMS}, PERMISSION_REQUEST_CODE);
    }

    public static void OpenSettingScreen(Context context)
    {

        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", context.getPackageName(), null);
        intent.setData(uri);
        ((Activity)  context).startActivityForResult(intent, PERMISSION_REQUEST_CODE);

    }


    public static void CheckResult(Context context, String permissions[], int[] grantResults)
    {
//        boolean CallPhone = grantResults[0] == PackageManager.PERMISSION_GRANTED;
//        boolean ReadContact = grantResults[1] == PackageManager.PERMISSION_GRANTED;
//        boolean ReadExternalStorage = grantResults[2]== PackageManager.PERMISSION_GRANTED;
        boolean SendSms = grantResults[3]== PackageManager.PERMISSION_GRANTED;
//        boolean RecordAudio = grantResults[4]== PackageManager.PERMISSION_GRANTED;
//        boolean addVoiceMail = grantResults[5]== PackageManager.PERMISSION_GRANTED;
//        boolean PhoneState = grantResults[6]== PackageManager.PERMISSION_GRANTED;


        if (SendSms  ) {


        } else {



            PermissionUtils.OpenSettingScreen(context);

        }

    }


}
